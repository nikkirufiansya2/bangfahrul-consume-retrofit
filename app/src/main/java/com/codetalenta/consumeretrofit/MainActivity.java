package com.codetalenta.consumeretrofit;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    EditText editTextUsername;
    EditText editTextPassword;
    Button submit;
    ApiService apiService;
    Session session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editTextUsername = findViewById(R.id.username);
        editTextPassword = findViewById(R.id.password);
        submit = findViewById(R.id.submit);
        session = new Session(MainActivity.this);
        if (session.getBoolean("login")){
            startActivity(new Intent(MainActivity.this, MenuUtama.class));
        }
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });
    }

    private void login(){
        apiService = UrlApi.getApiService();
        String username = editTextUsername.getText().toString();
        String password = editTextPassword.getText().toString();
        Call<ResponseBody> call = apiService.login(new LoginData(username,password));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200){
                    if (response.isSuccessful()){
                        try {
                            String respon = response.body().string();
                            JSONObject jsonObject = new JSONObject(respon);
                            String token = jsonObject.getString("token");
                            session.add("token", "Bearer" + " "+token);
                            session.add("login", true);
                            startActivity(new Intent(MainActivity.this, MenuUtama.class));
                        } catch (IOException | JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }else if (response.code() == 400){
                    System.out.println("Bad request");
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }
}