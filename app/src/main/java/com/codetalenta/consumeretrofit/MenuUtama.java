package com.codetalenta.consumeretrofit;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MenuUtama extends AppCompatActivity {
    Session session;
    ApiService apiService;
    RecyclerView recyclerView;
    FloatingActionButton buttonAdd;
    List<MhsModel> mhsModels = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_utama);
        apiService = UrlApi.getApiService();
        session = new Session(MenuUtama.this);
        System.out.println(session.getString("token"));
        recyclerView = findViewById(R.id.recycler_view);
        buttonAdd = findViewById(R.id.floatingActionButton);
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MenuUtama.this, InsertData.class));
            }
        });
        getData();
    }

    private void getData() {
        apiService.getMahasiswa(session.getString("token")).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String respon = response.body().string();
                    JSONArray jsonArray = new JSONArray(respon);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject object = jsonArray.getJSONObject(i);
                        mhsModels.add(new MhsModel(object.getInt("id"),object.getString("nama"), object.getString("email")));
                    }
                    AdapterMhs adapterMhs = new AdapterMhs(MenuUtama.this, mhsModels);
                    recyclerView.setLayoutManager(new GridLayoutManager(MenuUtama.this, 2));
                    recyclerView.setLayoutManager(new LinearLayoutManager(MenuUtama.this));
                    recyclerView.setAdapter(adapterMhs);
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }


}